package bg.tu_varna.sit.MainClasses;

import java.util.ArrayList;

public class Hall {
    private int hall;
    private int numberOfSeats;
    private ArrayList<Performance> performances;

    public Hall(int hall, int numberOfSeats) {
        this.hall = hall;
        this.numberOfSeats = numberOfSeats;
        this.performances = new ArrayList<>();
    }

    public void addPerformance(Performance performance){
        this.performances.add(performance);
    }

    public int getHall() {
        return hall;
    }

    public void setHall(int hall) {
        this.hall = hall;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }
}

}
