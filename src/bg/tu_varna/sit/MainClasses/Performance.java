package bg.tu_varna.sit.MainClasses;

import java.time.LocalDate;
import java.util.ArrayList;

public class Performance {
    private LocalDate date;
    private int hall;
    private String name;
    private ArrayList<Ticket> reserverdTickets;

    public Performance(LocalDate date, int hall, String name) {
        this.date = date;
        this.hall = hall;
        this.name = name;
        this.reservedTickets = new ArrayList<>();
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getHall() {
        return hall;
    }

    public void setHall(int hall) {
        this.hall = hall;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTicket(Ticket ticket){
        this.reservedTickets.add(ticket);
        System.out.println("Successfully added ticket!");
    }

}

}
