package bg.tu_varna.sit.MainClasses;


import java.util.ArrayList;

public class Storage {
    private ArrayList<Hall> halls;

    public Storage() {
        this.halls = new ArrayList<>();
    }
    public ArrayList<Hall> returnHalls(){
        return this.halls;
    }
    public void initializeData(){
        Hall firstHall = new Hall(1,30);
        Hall secondHall = new Hall(2,30);
        Hall thirdHall = new Hall(3,30);
        Hall fourthHall = new Hall(4,30);
        this.halls.add(firstHall);
        this.halls.add(secondHall);
        this.halls.add(thirdHall);
        this.halls.add(fourthHall);
    }
}


